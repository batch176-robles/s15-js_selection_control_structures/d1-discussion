console.log("Hello World!")
// alert("Hello, Batch 176!")

// Assignment Operator
// Basic Assignment Operator (=) *use in assigning variable*
let assignmentNumber = 8;

// Additional Assignment Operator (+=)
// assignmentNumber = assignmentNumber + 2
// console.log(assignmentNumber)

assignmentNumber += 2;
console.log(assignmentNumber);


// Subtraction (-=) / Multiplication (*=) / Division Assignment (/=) operator 
assignmentNumber -= 2;
console.log(assignmentNumber); //8

assignmentNumber *= 2;
console.log(assignmentNumber); //16

assignmentNumber /= 2;
console.log(assignmentNumber); //8

// Arithmetic Operators
let x = 1397;
let y = 7831;

// Addition Operator
let sum = x + y;
console.log(sum);

// Subtraction Operator
let difference = y - x;
console.log(difference)

// Multiplication Operator
let product = x * y;
console.log(product);

// Division Operator
let quotient = y / x;
console.log(quotient);

// Modulo Operator
let remainder = y % x;
console.log(remainder);

// Multiple Operator
// PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Substraction)

let mdas = 1 + 2 - 3 * 4 / 5;
//1 + 2 - 12 / 5
console.log(mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

// let pemdas2 = 2 + 4 - 2 ** (10 / 5);
// console.log(pemdas2);

// Exponential = 2 ** 2 (2 raise to the 2nd power)


// Increment and Decrement
let z = 1;

// Pre-increment
let increment = ++z;
console.log(increment); // 2
console.log(z); // 2

// Post-increment
increment = z++;
console.log(increment); //2
console.log(z); //3

// Pre-increment
let decrement = --z;
console.log(decrement); // 2
console.log(z); // 2

// Post-increment
decrement = z--;
console.log(decrement); // 2
console.log(z); // 2

// What happens if we add string and number?
let numA = '10';
let numB = 12;

// Type Coercion
let coercion = numA + numB;
console.log(coercion);
console.log(typeof numA);
console.log(typeof numB);
console.log(typeof coercion);

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);

// Adding Boolean and Number
// true = 1
// false = 0

let numE = true + 1;
console.log(numE);

let numZ = null + undefined; // NaN = not a number
console.log(numZ);

let numY = true * 5; // 5
console.log(numY);

let numW = undefined + 'Hello'; // undefinedHello
console.log(numW);
console.log(typeof numW);

// Comparison Operators
// Equality Operator (==) - checks whether the operands are equal or have the same content
// Returns a boolean value
let juan = 'juan';

console.log(1 == 1); //true
console.log(1 == "1"); //true
console.log(1 == 2); //false
console.log(0 == false); //true
console.log('juan' == 'juan'); //true
console.log('juan' == juan); //true

// Inequality Operator (!=) - checks whether the operands are NOT equal / have different content
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != "1"); //false
console.log(0 != false); //false
console.log("juan" != "juan"); //false
console.log("juan" != juan); //false


// Strict Equality Operator (===) checks whether the operands are equal or have the same content and data type

console.log(1 === 1); //true
console.log(1 === "1"); //false
console.log(1 === 2); //false
console.log(0 === false); //false
console.log("juan" === "juan"); //true
console.log("Juan" === "juan"); //false
console.log("juan" === juan); //true


// Strict Inequality Operator (!==) checks where the operands are NOT equare or have different content and data type

console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== "1"); //true
console.log(0 !== false); //true
console.log(null !== undefined); //false
console.log("juan" !== 'juan'); //false
console.log("juan" !== juan); //true

// Logical Operators
let isLegalAge = true;
let isRegistered = false;

// AND Operator(&&) returns true if all operands are true

let allRequirements = isLegalAge && isRegistered;
console.log(allRequirements); //false


// OR Operator(||) returns true if one of the operands is true
let someRequirements = isLegalAge || isRegistered;
console.log(someRequirements);

// NOT Operator (!)
let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsNotMet);

//Mini Activity:

/* Without using console, try to analyze the logic manually

    let isTrue = true
    let isFalse = false

    let isTralse = !(isTrue || isFalse && isFalse)
                   isFalse && isFalse = false
                   isTrue || false = true
                   !(true) = false
*/

// Control Structures
// - sorts out whether the statement/s are to be executed based on the condition, whether its true or false

/*
    // if else statement
    // switch statement
    */


// if .. else statement
/*
    Syntax:
        if (condition) {
            statement
        } else {
            statekejt
        }
*/


// if statement - executes a statement if a specified condition is true
//              - can stand alone without the else statement
/* if (condition) {
    code block
} 
*/

/*  < - less than
    > - greater than
    <= - less than or equal
    >= - greather than or equal
*/

let numG = -1;


if (numG < 0) {
    console.log("Hello!");
}

if (false == "1") {
    console.log("This statement will not be printed")
}   //will not execute due to condition not met


if (false == "1") {
    console.log("This statement will not be printed")
}

// else if clause
/* 
    - Executes a statement if previous conditions are false and if the specified condition is true
    - The "else if" clause is optionoal and can be added to capture additional conditions
*/

let numH = 1;

if (numG > 0) {
    console.log("Hello!")
} else if (numH > 0) {
    console.log("World!");
}

// else statement
/* 
    - Executes a statement if all other conditions are false
    - The "else" statement is optional and can be added to capture any other result to change the flow of a program.
*/

// if (numG > 0) {
//     console.log("Hello")
// } else if (numH = 0) {
//     console.log("World")
// } else {
//     console.log("Again!");
// }

// let age = prompt("Enter your age:");

// if (age <= 18) {
//     console.log("Not allowed to drink")
// } else {
//     console.log("Matanda ka na, Shot na PARE")
// }

/* 
    Mini Activity:
    Create a conditional statement that if height is below 150, display "Did not passesd the minimum height requirement." If above 150, display "Passed the minimum height requirement"

    Stretch Goal:
        Put it inside a function.
*/
// function height() {
//     let h1 = prompt("Enter your height")
//     if (h1 < 150) {
//         console.log("Did not passed the minimum height requirement")
//     } else {
//         console.log("Passed the minimum height requirement")
//     }
// }
// height();

// if, else if, and else statement with functions

let message = "No message.";
console.log(message);

function determineTyphoonIntensity(windSpeed) {
    if (windSpeed < 30) {
        return 'Not a Typhoon yet.'
    } else if (windSpeed <= 61) {
        return 'Tropical Depression detected.'
    } else if (windSpeed >= 62 && windSpeed <= 88) {
        return 'Tropical Storm detected.'
    } else if (windSpeed >= 89 && windSpeed <= 117) {
        return 'Severe Tropical Storm detected.'
    } else {
        return 'Typhoon detected'
    }
}

message = determineTyphoonIntensity(70);
console.log(message);

if (message == "Tropical Storm detected.") {
    console.warn(message);
}

// Truthy and Falsy
/* 
    In JS a "truthy" value is a value that is considered true when encountered in a Boolean context
    - Values are considered true unless defined otherwise

    - Falsy Values
        1. false
        2. 0
        3. -0
        4. ""
        5. null
        6. undefined
        7. NaN (not a number)
*/

// Truthy
if (true) {
    console.log("Truthy");
}

if (1) {
    console.log("Truthy");
}

if ([]) {
    console.log("Truthy");
}

// Falsy
if (false) {
    console.log("Falsy");
}

if (0) {
    console.log("Falsy");
}

// Conditional (Ternary) Operator
/* 
    It takes in 3 operands
    1. condition
    2. expression to execute if the condition is true
    3. expression to execute if the condition is false

    Syntax:
        (expression) ? ifTrue : ifFalse
*/

let ternaryResult = (1 < 18) ? true : false;
// let ternaryResult = (1 < 18) ? {true} : {false};
console.log(ternaryResult)

let myAge = 20;
myAge >= 18 ? console.log("IF") : console.log("ELSE");


// Sample with nested

// let ternaryResultb = (1 > 18) ? console.log("True1")
//     : (3 < 5) ? console.log("True2")
//         : console.log("False1")

// function isofLegalAge() {
//     name = "John";
//     return 'You are of the legal age limit'
// }

// function isUnderAge() {
//     name = "Jane";
//     return 'You are under the age limit'
// }

// let age1 = parseInt(prompt("What is your age?"))
// console.log(age1);
// let legalAge = (age1 > 18) ? isofLegalAge() : isUnderAge()
// console.log(legalAge + ", " + name);

// Switch Statement
/* 

    Syntax:
        Switch (expression) {
            case value:
                statement;
                break;
        }
*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day)

switch (day) {
    case 'monday', 'mon', 'lunes':
        console.log("The color of the day is red");
        break;
    case 'tuesday':
        console.log("The color of the day is orange");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow");
        break;
    case 'thursday':
        console.log("The color of the day is green");
        break;
    case 'friday':
        console.log("The color of the day is blue");
        break;
    case 'saturday':
        console.log("The color of the day is indigo");
        break;
    case 'sunday':
        console.log("The color of the day is violet");
        break;
    default:
        console.log("Please input a valid day");
        break;
}

// Try Catch Finally Statement

function showIntensityAlert(windSpeed) {
    try {
        alerat(determineTyphoonIntensity(windSpeed))
    }

    catch (error) {
        console.warn(error.message)
    }
    finally {
        alert("Intensity updates will show new alert.")
    }
}

showIntensityAlert(56)